// ==UserScript==
// @name        rh-jira-dismiss-announcement
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description Allows to dismiss the annoying announcement banner until it changes
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    if (document.querySelector('#announcement-banner') != null) {
        var dismissed = window.localStorage.getItem('announcementBannerDismissedContent');
        var current = document.querySelector('#announcement-banner > div').innerHTML;

        if (dismissed == null || dismissed != current) {
            var dismissbutton = document.createElement('a');

            dismissbutton.innerHTML = '[dismiss]';
            dismissbutton.addEventListener('click', (e) => {
                window.localStorage.setItem('announcementBannerDismissedContent', current);
                document.querySelector('#announcement-banner').style = 'display: none;';
                e.stopPropagation();
            });

            document.querySelector('#announcement-banner > div').appendChild(dismissbutton);
        } else {
            document.querySelector('#announcement-banner').style = 'display: none;';
        }
    }
})();
