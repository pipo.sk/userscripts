// ==UserScript==
// @name        rh-workday-workhours
// @namespace   Violentmonkey Scripts
// @match       https://wd5.myworkday.com/redhat/*
// @grant       none
// @version     1.1
// @author      Peter Krempa
// @description 2023/10/03
// ==/UserScript==

(function() {
    "use strict";

    function getWorkhours(d) {
        const month = d.getMonth();
        let workhours = 0;

        for (let i = 1; i <= 31; i++) {
            d.setDate(i);

            if (d.getMonth() != month)
                break;

            switch (d.getDay()) {
            case 0:
            case 6:
                break;

            default:
                workhours += 8;
                break;
            }
        }

        return workhours;
    }

    /* Note that many bits in this function are fragile and thus the script
     * may stop working if Workday changes the element layout. This script
     * works only for the English UI language version with either CZ or US
     * date formatting locale */
    function appendWorkhoursToTimesheetSubmit() {
        if (!document.title.startsWith('Submit Time') ||
            document.querySelector('#customWorkhours'))
            return;

        /* locate the headings and list items we'll look through */
        const lists = document.querySelectorAll('li');
        const h2 = document.querySelectorAll('h2');
        const h3 = document.querySelectorAll('h3');
        /* I hate JavaScript */
        const headings = Array.from(h2).concat(Array.from(h3));

        if (lists == null || headings == null) {
            console.log('rh-workday-workhours: Missing li or h2/h3 elements');
            return;
        }

        /* find the date of current timesheet in headings */
        let dateObj = null;

        for (const h of headings) {
            if (h.textContent.startsWith('Total for')) {
                const fullDate = h.textContent.replace('Total for ', '');
                const matchEN = fullDate.match(/([A-Za-z]+).*(\d\d\d\d)$/);
                const matchCZ = fullDate.match(/^1\..\d\d\. (\d+)\. (\d\d\d\d)$/);

                if (matchEN != null) {
                    dateObj = new Date(matchEN[1] + " 1, " +  matchEN[2]);
                }

                if (matchCZ != null) {
                    dateObj = new Date("Jan 1, " + matchCZ[2]);
                    dateObj.setMonth(Number(matchCZ[1]) - 1);
                }

                if (dateObj == null) {
                    console.log('rh-workday-workhours: failed to parse date ' + fullDate);
                    return;
                }

                break;
            }
        }

        if (dateObj == null) {
            console.log('rh-workday-workhours: failed to find date in page');
            return;
        }

        /* Find the appropriate element to put the workhours in */
        let reqHoursElem = null;
        let reqHoursParent = null;
        let totalHours = 0;

        for (const list of lists) {
            if (list.textContent.startsWith('Total Hours')) {

                reqHoursElem = list.cloneNode(true);
                reqHoursParent = list.parentElement;

                totalHours = Number(list.querySelector('.wd-Text').textContent);
                break;
            }
        }

        if (reqHoursElem == null) {
            console.log('rh-workday-workhours: failed to find target element');
            return;
        }

        /* process the date and inject it into the page */
        const workhours = getWorkhours(dateObj);

        reqHoursElem.querySelector('label').textContent = 'Required Hours:';
        reqHoursElem.querySelector('.wd-Text').textContent = workhours.toString();
        reqHoursElem.id = 'customWorkhours';

        if (totalHours != workhours) {
            reqHoursElem.style.backgroundColor = 'red';
        } else {
            reqHoursElem.style.backgroundColor = 'green';
        }

        reqHoursParent.appendChild(reqHoursElem);
    }

    let mut = new MutationObserver(appendWorkhoursToTimesheetSubmit);
    mut.observe(document.documentElement, { subtree: true, childList: true });
})();
