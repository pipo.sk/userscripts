// ==UserScript==
// @name        rh-adp
// @namespace   Violentmonkey Scripts
// @match       https://portal0012.globalview.adp.com/gvfrmwk3/redhat.home
// @version     1.1
// @author      Peter Krempa
// @grant       GM.addStyle
// @description 2021/02/15
// @run-at      document-end
// ==/UserScript==

(function () {
    // Directly load 'pay' page
    if (window.sessionStorage.getItem('firstVisit') == null) {
        window.sessionStorage.setItem('firstVisit', 'yes');
        window.location = 'https://portal0012.globalview.adp.com/gvfrmwk3/redhat.home#/pay';
    }

    /* hide menu */
    $('#side-toggle').click();
})();

GM.addStyle (`
    /* remove background and widen to full browser width */
    .stage {
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAC4jAAAuIwF4pT92AAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==);
        width: 100%
    }

    .stage .framework-container {
        width: 100%;
    }

    .stage .page-content {
        width: 100%;
    }

    .animate {
        transition: none;
    }
` );
