// ==UserScript==
// @name        RH-slack autologin and fix navigation to file after SSO login
// @namespace   Violentmonkey Scripts
// @match       https://redhat.enterprise.slack.com/*
// @grant       none
// @version     1.0.0
// @author      Peter Krempa
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    /* click the "SSO Login" button */
    if (document.location.href.startsWith('https://redhat.enterprise.slack.com/?redir')) {
        var ssologin = document.querySelector("a[id='enterprise_member_guest_account_signin_link_Red Hat SSO']");

        if (ssologin != null) {
            ssologin.click();
        }
    }

    /* fix the navigation once logged in */
    if (document.location.href.startsWith('https://redhat.enterprise.slack.com/files-pri/')) {
        document.location.host = 'files.slack.com';
    }
})();
