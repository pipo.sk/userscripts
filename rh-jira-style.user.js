// ==UserScript==
// @name        rh-jira-style
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.4
// @author      Peter Krempa
// @description A dramatic re-style of Jira. Makes eyes bleed less.
// ==/UserScript==

GM.addStyle(`
/* variables definition */
    :root {
        /* corners - select rounded (first) or not rounded (second) */
        --pkre_border_radius: 6px;
        --pkre_border_radius: 0px;

        /* colors - main */
        /* select default (first) or dark (second) */
        --pkre_color_main_background: var(--aui-page-background);
        --pkre_color_main_background: color-mix(in srgb, var(--aui-page-background) 77%, var(--aui-body-text) 23%);
        /* select default (first) or dark (second) */
        --pkre_color_sidebar_background: var(--aui-sidebar-bg-color);
        --pkre_color_sidebar_background: color-mix(in srgb, var(--aui-page-background) 71%, var(--aui-body-text) 29%);

        /* colors - comments, private comments, modules */
        --pkre_color_comment_body_background: var(--aui-page-background);
        --pkre_color_comment_heading_background: var(--aui-border);
        --pkre_color_comment_border: var(--pkre_color_comment_heading_background);

        --pkre_color_private_body_background: var(--aui-message-error-bg-color);
        --pkre_color_private_heading_background: color-mix(in srgb, var(--pkre_color_private_body_background) 80%, var(--aui-body-text) 20%);
        --pkre_color_private_border: var(--pkre_color_private_heading_background);

        --pkre_color_module_body_background: var(--aui-page-background);
        --pkre_color_module_heading_background: var(--aui-border);
        --pkre_color_module_border: var(--aui-border-strong);

        /* dark border around comments for better visual separation, disable to
         * use a lighter bugzilla-like border for comments */
        --pkre_color_comment_border: var(--pkre_color_module_border);
        --pkre_color_private_border: var(--pkre_color_module_border);

        /* colors - 'Activity' section */
        /* select the same as modules (first) or hidden (second) */
        --pkre_color_activity_border: var(--pkre_color_module_border);
        --pkre_color_activity_border: var(--aui-tabs-tab-border-color);
        /* select the same as modules (first) or hidden (second) */
        --pkre_activity_border_style: solid;
        --pkre_activity_border_style: dashed;

        /* computed from variables above */
        --pkre_border_top_radius: var(--pkre_border_radius) var(--pkre_border_radius) 0 0;
        --pkre_border_bottom_radius: 0 0 var(--pkre_border_radius) var(--pkre_border_radius);
    }

/* Removal of useless elements */

    /* Remove 'public project' warnings */
    #rppciinfo,
    #warn-attachment-msg,
    #rppiinfo
    {
        display: none;
    }

    /* remove collapse buttons from comments (comments are expanded on-load) */
    .activity-comment > .actionContainer > .action-head > h3 > button
    {
        display: none;
    }

    /* remove other collapse buttons  (clicking the heading collapses) */
    .toggle-header > .toggle-title > svg
    {
        display: none !important;
    }

    .toggle-header > .toggle-title
    {
        background-color: var(--pkre_color_module_heading_background);
    }

    .aui-toggle-header-button-label
    {
        position: static !important;
        padding-left: 0.5em;
    }

    /* hide header of 'Activity' section completely */
    #activitymodule_heading
    {
        display: none;
    }

/* un-stick and de-uglify comment adding */
    #addcomment
    {
        position: static !important;
        padding-bottom: 0px !important;
    }

    #addcomment-inner
    {
        padding-left: 0px !important;
    }

    /* hide hint */
    #addcomment .hint-container
    {
        display: none;
    }

/* styling changes - comments */

    /* border and padding/margins */
    .activity-comment
    {
        border: 1px var(--pkre_color_comment_border) solid;
        border-radius: var(--pkre_border_radius);
        padding: 0em;
        margin-bottom: 1em;
    }

    /* disable padding on internal container */
    .activity-comment > .actionContainer
    {
        padding: 0px;
    }

    /* gray background for comment header */
    .activity-comment > .actionContainer > .action-head
    {
        background-color: var(--pkre_color_comment_heading_background);
        border-radius: var(--pkre_border_top_radius);
    }

    /* white background for comment body and links */
    .activity-comment > .actionContainer > .action-body,
    .activity-comment > .actionContainer > .action-links
    {
        background-color: var(--pkre_color_comment_body_background);
        border-radius: var(--pkre_border_bottom_radius);
    }

    /* pink-ish background for private comments */
    .activity-comment > .actionContainer:has(span.aui-iconfont-locked) > .action-body,
    .activity-comment > .actionContainer:has(span.aui-iconfont-locked) > .action-links
    {
        background-color: var(--pkre_color_private_body_background);
    }
    .activity-comment:has(span.aui-iconfont-locked) {
        border-color: var(--pkre_color_private_border);
    }
    .activity-comment > .actionContainer > .action-head:has(span.aui-iconfont-locked)
    {
        background-color: var(--pkre_color_private_heading_background);
    }

    /* space out contents */
    .activity-comment > .actionContainer > .action-head,
    .activity-comment > .actionContainer > .action-body,
    .activity-comment > .actionContainer > .action-links
    {
        margin-top: 0px;
        padding: 0.3em 0.5em 0.3em 0.5em;
    }

/* styling changes */

    /* left sidebar darkening */
    .aui-sidebar-wrapper {
        border-color: var(--pkre_color_sidebar_background) !important;
        background-color: var(--pkre_color_sidebar_background) !important;
    }
    .aui-sidebar-footer {
        border: none !important;
    }

    /* override padding before modules */
    .issue-body-content .module > .mod-header + .mod-content
    {
        padding-left: 0px;
    }

    /* main background darkening */
    #issue-content,
    .issue-header,
    .issue-view,
    .split-view > .detail-panel
    {
        background-color: var(--pkre_color_main_background) !important;
    }

    /* module header fixing */
    .mod-header
    {
        padding: 0.3em 0.5em 0.3em 0.5em !important;
        background-color: var(--pkre_color_module_heading_background);
        border-radius: var(--pkre_border_top_radius);
    }

    .mod-header > h4
    {
        background-color: inherit !important;
    }

    /* module content fixing */
    .mod-content
    {
        margin-top: 0px !important;
        padding: 0.5em !important;
    }

    #attachmentmodule .mod-content.issue-drop-zone
    {
        margin: 5px !important;
        border-radius: var(--pkre_border_top_radius);
    }


    /* border, padding and white background for all modules */
    .module
    {
        padding-top: 0px !important;
        background-color: var(--pkre_color_module_body_background);
        border-top: 1px var(--pkre_color_module_border) solid !important;
        border: 1px var(--pkre_color_module_border) solid !important;
        border-radius: var(--pkre_border_radius);
    }

    /* override above for activity module */
    #activitymodule
    {
        border-top: 1px var(--pkre_color_activity_border) var(--pkre_activity_border_style) !important;
        border: 1px var(--pkre_color_activity_border) var(--pkre_activity_border_style) !important;
        background-color: var(--pkre_color_main_background);
    }

    /* override above for activity and addcoment */
    #addcomment-inner,
    #addcomment
    {
        border-top: none !important;
        border: none !important;
        background-color: var(--pkre_color_main_background);
    }


    /* issue header background and border */
    .issue-header-content > .aui-page-header
    {
        background-color: var(--aui-page-background) !important;
        padding: 0.5em !important;
        padding-bottom: 0.5em !important;
        margin: 15px 20px 0px;
        border: 1px var(--pkre_color_module_border) solid;
        border-radius: var(--pkre_border_radius);
    }

    /* pink-ish background when editing private comment */
    #addcomment-inner .jira-wikifield
    {
        background-color: var(--pkre_color_comment_body_background);
    }

    #addcomment-inner:has(span.redText) .jira-wikifield
    {
        background-color: var(--pkre_color_private_body_background);
    }

    /* overrides for the editor to facilitate background change */
    body#tinymce,
    .tox .tox-edit-area__iframe,
    #comment-wiki-edit > textarea
    {
        background: inherit !important;
    }

` );
