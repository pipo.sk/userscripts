Userscripts
===========

Collection of userscripts to make corporate tools slightly less annoying to
use.

rh-jira
-------

`rh-jira-comments <rh-jira-comments.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Automatically load all comments for an issue.

`rh-jira-comment-reply <rh-jira-comment-reply.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add a ``[reply]`` button to each comment that opens the editor for posting new
comment pre-filled with the quoted comment. Also works multiple times to quote
multiple comments as in bugzilla.

`rh-jira-timestamps <rh-jira-timestamps.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Replace the 'relative' timestamps with the actual timestamp
in local timezone and 24h format.

`rh-jira-description-edit-disable <rh-jira-description-edit-disable.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Don't switch to the WYSIWYG editor for the ``Description`` field when clicking
into it anywhere, but just when clicking the edit icon pencil on the side.

`rh-jira-sidebar <rh-jira-sidebar.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Move the 'users' sidebar next to the 'Details' section without
taking space next to the rest of the page. Useful for users of
narrow browser windows (portrait monitor).

`rh-jira-monospace-comments <rh-jira-monospace-comments.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Override fonts for comments and description to use the default
monospace fonts. Useful to be able to read command outputs.

`rh-jira-autologin-public <rh-jira-autologin-public.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Initiate login when navigating to a public issue while logged out. In such case
Jira displays the issue and doesn't force you to log in which is counterproductive
if you then want to make changes.

`rh-jira-sso-preserve-navigation <rh-jira-sso-preserve-navigation.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Store and preserve the redirection when logging in via SSO. Useful if SSO fails
e.g. because you're already logged in to preserve the navigation inside Jira.

See below for scripts to recover from SSO failure.

`rh-jira-remove-emoji-reactions <rh-jira-remove-emoji-reactions.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove 'emoji' reactions for comments. Both the button for adding the reaction
and the reactions itself.

`rh-jira-expand-custom-field-tabs <rh-jira-expand-custom-field-tabs.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Always display all "custom field" tabs and make them look as other fields.

`rh-jira-links <rh-jira-links.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Always expand the pointlessly abbreviated 'Issue links' section.

`rh-jira-style <rh-jira-style.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Restyle of some aspects of the UI to increase visual separation of comments and
other usability tweaks.

`rh-jira-remove-planning-poker <rh-jira-remove-planning-poker.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove the "Easy Agile Planning Poker" module which wastes space above comments.


`rh-jira-remove-intelli-tldr <rh-jira-remove-intelli-tldr.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove the "IntelliuTLDR" pane. Life is too short to verify AI slop.


`rh-jira-remove-rainbow-code <rh-jira-remove-rainbow-code.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Displays anything under ``{code:rainbow}`` using default coloring for code
blocks instead of the rainbow puke.


`rh-jira-comment-collapse-disable <rh-jira-comment-collapse-disable.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Don't collapse comments when clicking on their title. Use just the arrow button
if visible.

`rh-jira-dismiss-announcement <rh-jira-dismiss-announcement.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add possibility to dismiss annoying announcement banner until it changes content.


SSO login
---------

`rh-sso-autologin <rh-sso-autologin.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Automatically confirm email address to automate SSO login flow into Jira. Don't
forget to modify the email address.

`rh-sso-autologin-error <rh-sso-autologin-error.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Recover from login failures by navigating back to Jira. The complementary script
to restore navigation inside Jira is needed.

`rh-source-autologin <rh-source-autologin.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Automatically click the employee login button when a 'source' session doesn't
exist to avoid the need for pointless user interaction.

`rh-sso-remove-news <rh-sso-remove-news.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove the "NEWS" background image from the SSO page.

other
-----

`rh-slack-fix-navigation-to-file <rh-slack-fix-navigation-to-file.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For users of external slack clients this script fixes awful UX when clicking on
a link created from an inline attachment. The script clicks the 'SSO login'
button if needed and after login fixes the host do display the attachment.

`rh-workday <rh-workday.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bypass the pointless 15 minute session timeout. Note that a server side 24h
session duration still applies.

`rh-workday-workhours <rh-workday-workhours.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add a number of expected workhours to the table when submitting a timesheet for
verification that it was filled properly. Note that it works only with English
language set in workday and with CZ or US locales.

`rh-adp <rh-adp.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Open directly the more useful page in ADP and make the view wider.

`kaltura-no-reactions <kaltura-no-reactions.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove the annoying floating emoji reactions from kaltura events.


Userscript development
======================

Firefox doesn't allow addons to load local files so a workaround offered by
Tampermonkey is to load a file from a local http server. The local server
provided in ``server.py`` disables caching as that interferes with the updates.

1) Run the server::

 $ python server.py

2) open the local server page

 http://localhost:8080

3) click on script to develop

4) Select ``Track local file before this window is closed``

5) **DO NOT CLOSE THE WINDOW**

The script will be auto-loaded periodically so you can develop it in the local
editor.
