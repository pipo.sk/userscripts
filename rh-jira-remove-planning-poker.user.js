// ==UserScript==
// @name        rh-jira-remove-planning-poker
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description Remove planning poker module completely
// ==/UserScript==

GM.addStyle (`
    #eausm-planning-poker-issue-view-web-panel
    {
        display: none;
    }
` );
