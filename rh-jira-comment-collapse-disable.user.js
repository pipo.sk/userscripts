// ==UserScript==
// @name        rh-jira-comment-collapse-disable
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @version     1.0
// @author      Peter Krempa
// @description disable collapsing of comments when clicking the top bar
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    /* helper for eating 'click' event from the comment title and specially
     * for the 'livestamp' sub-element */
    function comment_collapse_click_cancel(e) {
        if (e.srcElement.classList.contains('action-details') ||
            e.srcElement.classList.contains('livestamp')) {
            e.stopPropagation();
        }

        return;
    }

    function install_comment_collapse_override() {

        let comments = document.querySelectorAll('.action-details:not(.flooded):not(.collapse-click-overridden)');

        if (comments) {
            for (const comment of comments) {
                comment.addEventListener('click', comment_collapse_click_cancel, true);
                comment.classList.add('collapse-click-overridden');
            }
        }
    }

    let mut = new MutationObserver(install_comment_collapse_override);
    mut.observe(document.documentElement, { subtree: true, childList: true });
    install_comment_collapse_override();

})();
