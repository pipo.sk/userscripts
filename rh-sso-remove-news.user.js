// ==UserScript==
// @name        rh-sso-remove-news
// @namespace   Violentmonkey Scripts
// @match       https://auth.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @run-at      document-start
// @description Remove the pointless 'news' image from the internal SSO
// ==/UserScript==
// Running at document-start prevents the image from loading altogether.

GM.addStyle (`

    body#sso-page
    {
        background: black !important;
    }
` );
