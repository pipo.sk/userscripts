// ==UserScript==
// @name        rh-jira-monospace-comments
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.2
// @author      Peter Krempa
// @description Make everything in comments and description monospace, in case someone pastes un-formatted logs.
// ==/UserScript==

GM.addStyle (`

/* Font stuff */

    /* Use browser-defined sans-serif font, instead of forced ones. */
    html
    {
        font-family: sans-serif;
    }

    /* Use monospace font for comment body and issue description. */
    .action-body,
    #description-val
    {
        font-family: monospace;
    }

    /* add background for originally monospace inline text
     * colors are taken from the 'code' block styling */
    .action-body tt
    {
        border: 1px solid #c1c7d0;
        border-radius: 3px 3px 3px 3px;
        background: #f4f5f7;
        padding-left: 0.2em;
        padding-right: 0.2em;
    }

    /* Remove 30 line size limitation for code blocks */
    pre
    {
        max-height: none !important;
    }
` );
