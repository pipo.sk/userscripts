// ==UserScript==
// @name        rh-jira-comment-reply
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     2.0
// @author      Peter Krempa
// @description Add link for replying to comments. Now with preserving comment visibility.
// @run-at      document-end
// ==/UserScript==

// Adds possibility to reply to comments, unfortunately just one level of quoting
// is possible as the jira-markup has the same tag for opening and closing markup
// thus adding additional quoting inverts the previous one


(function(){
    "use strict";

    /* https://stackoverflow.com/a/61511955 CC BY-SA 4.0, Yong Wang */
    function waitForElm(selector, doc) {
        return new Promise(resolve => {
            if (doc.querySelector(selector)) {
                return resolve(doc.querySelector(selector));
            }

            const observer = new MutationObserver(mutations => {
                if (doc.querySelector(selector)) {
                    observer.disconnect();
                    resolve(doc.querySelector(selector));
                }
            });

            // If you get "parameter 1 is not of type 'Node'" error, see https://stackoverflow.com/a/77855838/492336
            observer.observe(doc.body, {
                childList: true,
                subtree: true
            });
        });
    }


    /* Sets the visibility based on 'group' and updates the UI */
    function setVisibility(doc, group) {
        if (group == '') {
            doc.querySelector('#commentLevel').value = '';
            doc.querySelector('#currentLevel').innerHTML = 'Viewable by All Users';
        } else {
            /* lookup the value based on the visible string */
            let options = doc.querySelectorAll('#commentLevel option');

            for (const opt of options) {
                if (opt.text == group) {
                    doc.querySelector('#commentLevel').value = opt.value;
                    doc.querySelector('#currentLevel').innerHTML = 'Restricted to <span class="redText">' + opt.text + '</span> via [reply]';

                }
            }
        }
    }


    async function replyComment(cmt) {
        /* open the editor */
        document.querySelector('#footer-comment-button').click();

        /* This script uses the WYSIWYG editor to do the conversion from HTML
         * to jira markup for us. We need to switch to the editor first as it's
         * loaded lazily */
        let wysiwygbutton = await waitForElm('li[data-mode=wysiwyg] > button', document);
        wysiwygbutton.click();

        /* wait for the editor to appear */
        let editor = await waitForElm('#addcomment iframe', document);
        let editorbody = editor.contentDocument.querySelector('body');

        /* wait until the expected content in the editor becomes available as
         * otherwise our stuff would be overwritten */
        await waitForElm('body p', editor.contentDocument);

        /* visibility group */
        let groupEl = cmt.querySelector('.action-details .subText .redText');

        /* If the initial comment area is empty and the comment isn't restricted
         * we can reset the visibility */
        if (editorbody.textContent == '') {
            let group = '';

            if (groupEl != null)
                group = groupEl.textContent;

            setVisibility(document, group);
        } else {
            /* Do not override visibility if already restricted */
            if (groupEl != null &&
                document.querySelector('#commentLevel').value == '') {
                setVisibility(document, groupEl.textContent);
            }
        }

        /* mention the user replying to */
        let user = cmt.querySelector('a.user-hover');
        let replyuser = document.createElement('a');
        replyuser.href = user.href;
        replyuser.text = user.text;
        replyuser.classList.add('user-hover');
        replyuser.rel = user.rel;

        /* add anchor to the comment we're replying to */
        let replyref = document.createElement('a');
        replyref.href = '#' + cmt.id;
        replyref.text = cmt.id;

        let replynote = document.createElement('div');
        replynote.append('(In reply to ', replyuser, ' from ', replyref, ')');

        editorbody.appendChild(replynote);

        /* process the comment */
        for (const c of cmt.querySelector('.action-body').children) {
            /* The jira markup uses exactly the same tag for opening and closing
             * a markup block thus nesting is impossible */
            let n = c.cloneNode(true);

            /* do actual quoting for any non-quote block */
            if (n.nodeName != 'BLOCKQUOTE') {
                let q = document.createElement('blockquote');
                q.appendChild(n);
                n = q;
            }

            editorbody.appendChild(n);
        }

        /* drop the empty leading paragraph */
        if (replynote.previousElementSibling &&
            replynote.previousElementSibling.nodeName == 'P' &&
            replynote.previousElementSibling.textContent == '')
            replynote.previousElementSibling.remove();

        /* add empty paragraph after the comment which was inserted and move the
         * cursor to the new paragraph */
        let newp = editor.contentDocument.createElement('p');
        newp.innerHTML = '<br data-mce-bogus="1">';

        editorbody.appendChild(newp);

        /* this needs to be done in a timeout otherwise it doesn't work*/
        setTimeout(() => {
            let selection = editor.contentWindow.getSelection();
            let range = editor.contentDocument.createRange();

            /* Move the cursor to the new empty paragraph */
            range.selectNode(newp);
            range.setStart(newp, 0);
            range.collapse(true);

            selection.removeAllRanges();
            selection.addRange(range);

            /* scroll to the bottom of the editor */
            editor.contentWindow.scrollTo(0, editorbody.scrollHeight);
        });
    }

    /* add [reply] buttons to each comment */
    function addReplyButton() {
        let comments = document.querySelectorAll('.activity-comment:not(.reply-link-added)');

        for (const cmt of comments) {
            let details = cmt.querySelector('.action-details');
            let reply = document.createElement('a');

            reply.innerHTML = '[reply]';
            reply.classList.add('replycommentbutton');
            reply.addEventListener('click', (e) => {
                replyComment(cmt);
                e.stopPropagation();
            });

            details.appendChild(reply);

            cmt.classList.add('reply-link-added');
        }
    }

    let mut = new MutationObserver(addReplyButton);
    mut.observe(document.documentElement, { subtree: true, childList: true });
})();


/* perhaps right-align the [reply] link? */
GM.addStyle (`
` );
