// ==UserScript==
// @name        rh-jira-remove-rainbow-code
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description Somebody had the brilliant idea to color code with rainbow gradient instead of adding actually useful features.
// ==/UserScript==

GM.addStyle (`
   .code-rainbow {
      background: inherit;
      background-clip: unset;
      color: inherit;
   }
` );
