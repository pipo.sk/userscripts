// ==UserScript==
// @name        rh-jira-timestamps
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.1
// @author      Peter Krempa
// @description 2023/09/13
// @run-at      document-end
// ==/UserScript==


(function(){
    "use strict";

    function conv(num) {
        if (num < 10) {
            return "0" + num.toString();
        } else {
            return num.toString();
        }
    }

    /* helper function to format dates in reasonable format in local timezone */
    /* used to replace the 'relative' dates Jira does by default, and also
     * force a reasonable (24hour) format of time. (not user-configurable in Jira) */
    function fixDates() {
        let livestamps = document.querySelectorAll('.livestamp:not([properdate]');

        for (const stamp of livestamps) {
            let d = new Date(stamp.getAttribute('datetime'));
            let nd = conv(d.getFullYear()) + "-" + conv(d.getMonth() + 1) + "-" + conv(d.getDate());
            let nt = conv(d.getHours()) + ":" + conv(d.getMinutes());

            stamp.setAttribute('properdate', nd + " " + nt);
        }
    }

    let mut = new MutationObserver(fixDates);
    mut.observe(document.documentElement, { subtree: true, childList: true });

    fixDates();
})();

GM.addStyle (`
    .livestamp[properdate]
    {
        font-size: 0px;
    }

    .livestamp[properdate]::after
    {
        content: attr(properdate);
        font-size: 13px;
    }
` );
