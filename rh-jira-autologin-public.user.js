// ==UserScript==
// @name        rh-jira-autologin-public
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       none
// @version     1.0
// @author      Peter Krempa
// @description Initiate login if loading a issue not requiring login while logged out
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    let loginLink = document.querySelector('a.login-link');

    if (loginLink != null) {
        loginLink.click();
    }
})();
