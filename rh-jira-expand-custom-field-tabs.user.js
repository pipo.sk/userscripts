// ==UserScript==
// @name        rh-jira-expand-custom-field-tabs
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.2
// @author      Peter Krempa
// @description display all custom field tabs at once
// ==/UserScript==

GM.addStyle (`

    /* display all tabs at once, make them fit with the other fields */
    #customfieldmodule > #customfield-tabs > .tabs-pane
    {
        padding-left: 0px !important;
        padding-top: 0px !important;
        display: block !important;
    }

    /* hide the headers */
    #customfieldmodule > #customfield-tabs  > .tabs-menu
    {
        display: none !important;
    }

    /* add border to separate the custom fields */
    #customfieldmodule
    {
        border-top: 1px dashed #ccc;
        margin-top: 0.5em;
        padding-top: 0.5em;
    }
` );
