// ==UserScript==
// @name        Red Hat 'source' autologin
// @namespace   Violentmonkey Scripts
// @match       https://source.redhat.com/?signin*
// @grant       none
// @version     1.0.0
// @author      Peter Krempa
// @description 2023/10/02
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";
    document.querySelector('#samlsignin-submit-button').click();
})();
