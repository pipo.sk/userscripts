// ==UserScript==
// @name        rh-jira-remove-emoji-reactions
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description Remove emoji reactions for comments
// ==/UserScript==

GM.addStyle (`

    /* remove the whole emoji section */
    jira-comment-reactions
    {
        display: none !important;
    }

    /* remove 'dot' separator between 'Edit * Delete' and the now removed emoji stuff */
    .action-links__divider:has(+ jira-comment-reactions)
    {
        display: none !important;
    }
` );
