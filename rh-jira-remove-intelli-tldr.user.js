// ==UserScript==
// @name        rh-jira-remove-intelli-tldr
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description Remove 'IntelliTLDR' AI pane. Life is too short to read AI slop.
// ==/UserScript==

GM.addStyle (`
    #intellitldr-pane
    {
        display: none;
    }
` );
